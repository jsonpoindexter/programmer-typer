// Menu Options
export type Options = { [key in OptionType]: Selection[] }

export interface Option {
  type: OptionType
  selections: Selection[]
}

// Types of menu options supported (value used for HTML)
export enum OptionType {
  LANGUAGE = 'language',
  ROUND_TIME = 'round time',
}

export interface Selection {
  title: string
  value: any
  shortCut: ShortCut // keyboard shortcut for toggling 'selected'
  selected: boolean
  source?: LanguageSource
}

// Types of languages supported
export enum LanguageType {
  JAVA_SCRIPT = 'javascript',
  PYTHON = 'python',
}

export interface ShortCut {
  key: string
  titleIndex?: number // selection 'title' index to underline (indicate to users shortcut key)
}

export interface LanguageSource {
  urls: LanguageUrl[] // urls to req snippets from
  lastRandom?: number // last random so we dont repeat
}

export interface LanguageUrl {
  url: string
  startLine?: number // Start on a specific line of code [1 through code length]
}