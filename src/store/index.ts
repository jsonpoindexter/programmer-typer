import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { LanguageSource, LanguageType, Options, OptionType, Selection } from '@/store/types'

Vue.use(Vuex)

interface SetMenuPayload {
  type: OptionType
  index: number
}

interface State {
  typerState: string
  cpm: number
  snippetArr: string[][] // parse code snippet into 2D array (lines of words)
  options: Options // Menu Options
}

export default new Vuex.Store<State>({
  state: {
    typerState: '',
    cpm: 0,
    snippetArr: [],
    options: {
      [OptionType.LANGUAGE]: [
        {
          title: 'JavaScript',
          value: LanguageType.JAVA_SCRIPT,
          shortCut: { key: 'j', titleIndex: 0 },
          selected: false,
          source: {
            urls: [
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/send/master/server/routes/exists.js' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/send/master/postcss.config.js' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/send/master/server/initScript.js' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/send/master/server/amplitude.js' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/send/master/server/config.js' },
            ],
          },
        },
        {
          title: 'Python',
          value: LanguageType.PYTHON,
          shortCut: { key: 'p', titleIndex: 0 },
          selected: false,
          source: {
            urls: [
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/cpython/master/Python/makeopcodetargets.py' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/django/master/django/bin/django-admin.py' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/django/master/django/db/transaction.py' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/django/master/django/core/checks/messages.py' },
              { url: 'https://raw.githubusercontent.com/jsonpoindexter/django/master/django/core/checks/urls.py' },
            ],
          },
        },
      ],
      [OptionType.ROUND_TIME]: [
        { title: '120', value: 120, shortCut: { key: '1', titleIndex: 0 }, selected: false },
        { title: '60', value: 60, shortCut: { key: '6', titleIndex: 0 }, selected: false },
        { title: '30', value: 30, shortCut: { key: '3', titleIndex: 0 }, selected: false },
      ],
    },
  },
  mutations: {
    setTyperState(state, typerState: string): void {
      state.typerState = typerState
    },
    setCpm(state, cpm: number): void {
      state.cpm = cpm
    },
    setMenuOption({ options }, payload: SetMenuPayload) {
      const { type, index } = payload
      // Find the correct Option by type
      const selections = options[type]
      if (selections[index].selected) {
        // Set all selections's selections to false
        selections.forEach((selection: Selection) => (selection.selected = false))
      } else {
        // Set all selections's selections to false
        selections.forEach((selection: Selection) => (selection.selected = false))
        // Set appropriate selections's selection to true
        selections[index].selected = true
      }
    },
    setSnippetArr(state, snippetArr: string[][]): void {
      state.snippetArr = snippetArr
    },
  },
  actions: {
    async fetchSnippet(store, type: LanguageType): Promise<void> {
      const sourceArr: LanguageSource | undefined = store.state.options[OptionType.LANGUAGE].find(
        (selection: Selection) => {
          return selection.value === type
        },
      )!.source

      // Get randomized url for code snippet
      if (!sourceArr) throw new Error(`Error: Could not get language source array for '${type}' LanguageType`)
      else if (!sourceArr.urls.length) throw new Error(`Error: No source.urls specified for '${type}' LanguageType`)

      // Generate a random num that isn't the same as the last random num
      let randomNumber = Math.round(Math.random() * (sourceArr.urls.length - 1))
      while (sourceArr.urls.length > 1 && randomNumber === sourceArr.lastRandom)
        randomNumber = Math.round(Math.random() * (sourceArr.urls.length - 1))
      sourceArr.lastRandom = randomNumber
      // Fetch snippet
      const source = sourceArr.urls[randomNumber]
      try {
        const snippet = (await axios.get(source.url)).data
        // Parse snippet
        const snippetArray: string[][] = []
        snippet
          .split('\n')
          .slice(source.startLine ? source.startLine - 1 : 0)
          .forEach((line: string) => {
            // break down each line to word arrays
            if (line) {
              const words = line.split(' ')
              // break down each word adding a space at the end except for the last word (for Enter)
              // TODO: look into using regex instead (will keep white space)
              snippetArray.push(
                // dont add space after word on last
                words.map((word: string, index: number) => (index === words.length - 1 ? word : `${word} `)),
              )
            }
          })
        store.commit('setSnippetArr', snippetArray.slice(0, 12))
      } catch (err) {
        throw err
      }
    },
    retryRound(store): void {
      store.commit('setCpm', 0)
      store.commit('setTyperState', 'round')
    }
  },
  getters: {
    maxRoundTime({ options }): number {
      const selection = options[OptionType.ROUND_TIME].find((selections: Selection) => selections.selected)
      return selection ? selection.value : 0
    },
    language({ options }): LanguageType | null {
      const selection = options[OptionType.LANGUAGE].find((selections: Selection) => selections.selected)
      return selection ? selection.value : null
    },
  },
  modules: {},
})
